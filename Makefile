BIN     = tb
DEST    = /usr/local
MANDEST = $(DEST)/share/man

CFLAGS  = -Wall -Wextra -O2 -march=native -std=c89 -pedantic

OBJS    = textbox.o

.c.o:
	cc $(CFLAGS) -c $<

$(BIN): $(OBJS)
	cc $(CFLAGS) $^ -o $@ 

install:
	mkdir -p $(DEST)/bin
	cp -f $(BIN) $(DEST)/bin
	chmod 755 $(DEST)/bin/$(BIN)
	mkdir -p $(MANDEST)/man1
	cp -f $(BIN).1 $(MANDEST)/man1
	chmod 644 $(MANDEST)/man1/$(BIN).1

uninstall:
	rm -f $(DEST)/bin/$(BIN) $(MANDEST)/man1/$(BIN).1

clean:
	rm -f *.o $(BIN)

.PHONY: install uninstall clean
