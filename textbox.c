#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <locale.h>

#define VERSION            "0.1"
#define BOX_BORDER         '#'
#define TILE_TEXT_X        20
#define TILE_RIGHT_ARROW_X  5
#define TILE_LEFT_ARROW_X   5
#define TILE_UP_ARROW_X     3
#define TILE_DOWN_ARROW_X   3
#define BLOCK_Y             3
#define GRID_COL           80
#define GRID_LINES         30
#define FILE_LEN_LINE      80

enum {
	ERR_ARG,
	ERR_BEGIN
};

/* function prototypes */
void init_grid(void);
void tokenize(char *);
void analize_token_content(char *);
void create_tile_right_arrow(void);
void create_tile_left_arrow(void);
void create_tile_up_arrow(void);
void create_tile_down_arrow(void);
void create_tile_text(char *);
void print_grid(void);
void help(void);
void error_handle(int, int);

/* global variable */
int  grid_x;
int  grid_y;
char grid[GRID_LINES][GRID_COL];

int
main(int argc, char **argv)
{
	int i;

	/* no arguments */
	if (argc <= 1) {
		help();
	} else if (argc > 2) {
		error_handle(ERR_ARG, argc - 1);
	} else {
		/* get options */
		for (i = 1; i < argc; i++) {
			/* these two first options take no arguments */
			if (strcmp("-h", argv[i]) == 0) {
				help();
			} else if (strcmp("-v", argv[i]) == 0) {
				puts("textbox-"VERSION);
			/* read file and process content */
			} else {
				init_grid();
				tokenize(argv[1]);
				print_grid();
			}
		}
		exit(EXIT_SUCCESS);
	}

	/* unreachable */	
	fputs("Something goes very wrong", stderr);
	return EXIT_FAILURE;
}

void
init_grid(void)
{
	int i;
	int j;

	grid_x = 0;
	grid_y = 0;

	/* init grid with white spaces */
	for (i = 0; i < GRID_LINES; i++) {
		for (j = 0; j < GRID_COL; j++) {
			grid[i][j] = ' ';
		}
	}
}

void
tokenize(char *file)
{
	char  single_line[FILE_LEN_LINE];
	char *token;
	FILE *fp;

	fp = fopen(file, "r");

	while (fgets(single_line, FILE_LEN_LINE, fp) != NULL) {
		token = strtok(single_line, "|");
		while (token != NULL) {
			analize_token_content(token);
			token = strtok(NULL, "|");
		}
		grid_x = 0;
		grid_y = grid_y + BLOCK_Y;
	}
	fclose(fp);
}

void
analize_token_content(char *content)
{
	if (strcmp("=>", content) == 0) {
		create_tile_right_arrow();
	} else if (strcmp("<=", content) == 0) {

	} else if (strcmp("=V", content) == 0) {

	} else if (strcmp("V=", content) == 0) {

	} else if (strcmp("::", content) == 0) {
		return;
	} else if (strcmp("=sb=", content) == 0) {

	} else {
		if (strlen(content) == 0) {
			return;
		}
		create_tile_text(content);
	}
}

void
create_tile_right_arrow(void)
{
	int tile_limit_x;

	tile_limit_x = grid_x + TILE_RIGHT_ARROW_X;
	grid_y++;

	for (grid_x; grid_x < tile_limit_x - 1; grid_x++) {
		grid[grid_y][grid_x] = '=';
	}
	grid[grid_y][grid_x] = '>';

	grid_x++;
	grid_y--;
}

void
create_tile_text(char *content)
{
	int  tile_limit_x;
	char mid_of_box[TILE_TEXT_X];
	
	/* mid section of a text box */
	strncpy(mid_of_box, "# ", 3);	
	strncat(mid_of_box, content, TILE_TEXT_X - 4);
	strncat(mid_of_box, " #", 3);	
	printf("%s\n", mid_of_box);

	tile_limit_x = grid_x + TILE_TEXT_X;

	/* top border */
	for (grid_x; grid_x < tile_limit_x; grid_x++) {
		grid[grid_y][grid_x] = BOX_BORDER;
	}
	grid_x = grid_x - tile_limit_x;
	grid_y++;

	/* content of box */
	for (grid_x; grid_x < tile_limit_x; grid_x++) {
		grid[grid_y][grid_x] = mid_of_box[grid_x];
	}
	grid_x = grid_x - tile_limit_x;
	grid_y++;

	/* bottom border */
	for (grid_x; grid_x < tile_limit_x; grid_x++) {
		grid[grid_y][grid_x] = BOX_BORDER;
	}
	grid_y = grid_y - (BLOCK_Y - 1);
}

void
print_grid(void)
{
	int i;
	int j;

	for (i = 0; i < GRID_LINES; i++) {
		for (j = 0; j < GRID_COL; j++) {
			putchar((char)grid[i][j]);
		}
		puts("");
	}
}

/***************************/
/* Utils and Miscellaneous */
/***************************/

void
help(void)
{
	puts("Usage:");
	puts("tb [Options] file\n");
	puts("Options:");
	puts("-v, show version");
	puts("-h, show this menu");

	exit(EXIT_SUCCESS);
}

void
error_handle(int err_code, int arg)
{
	switch (err_code) {
		case ERR_ARG:
			fprintf(stderr, "Invalid number of arguments! %d "
			        "arguments instead of 1\n\n", arg);
			help();
			break;
		case ERR_BEGIN:
			fprintf(stderr, "Incorrect sintax, beginning of line"
			        "number %d\n", arg);
			break;
		default:
			break;
			        
	}
	exit(EXIT_FAILURE);
}
