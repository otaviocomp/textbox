################
# INSTALLATION #
################

To install the program enter the following commands:

make
make install

You can read more information about this program and all your options typing
"man textbox" to access the manual page of "textbox".

###############
# DESCRIPTION #
###############

This program puts your ordinary text inside a box.

#############
# UNINSTALL #
#############

To uninstall, just type:

make uninstall
